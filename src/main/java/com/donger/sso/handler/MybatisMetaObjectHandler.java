package com.donger.sso.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 * 审计字段自动填充
 *
 * @author aeizzz
 */
@Component
@Slf4j
public class MybatisMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start install fill ...");

        /*
          创建时间
          起始版本 3.3.0(推荐使用)
         */
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());

//        // 新增人，  判断当前登录对象  没有则是系统自动增加
//
//        if (Objects.isNull(user)) {
//            /*
//              创建人信息
//              起始版本 3.3.0(推荐使用)
//             */
//            this.strictInsertFill(metaObject, "createBy", String.class, "系统自动生成");
//        } else {
//            /*
//              创建人信息
//              起始版本 3.3.0(推荐使用)
//             */
//            this.strictInsertFill(metaObject, "createBy", String.class, user.getUsername());
//        }


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ...");
        /*
            更新时间
         */
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());

//        // 新增人，  判断当前登录对象  没有则是系统自动增加
//        UserDetail user = SecurityUtils.getUser();
//        if (Objects.isNull(user)) {
//            /*
//                更新人信息
//             */
//            this.strictInsertFill(metaObject, "updateBy", String.class, "系统自动生成");
//        } else {
//            /*
//                更新人信息
//             */
//            this.strictInsertFill(metaObject, "updateBy", String.class, user.getUsername());
//        }
    }
}
