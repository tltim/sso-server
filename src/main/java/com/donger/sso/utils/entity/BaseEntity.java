package com.donger.sso.utils.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * 数据库通用审计字典内容
 *
 * @author aeizzz
 */
@Setter
@Getter
public abstract class BaseEntity implements Serializable {

    @TableId
    @ColumnComment("id")
    private Long id;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    @ColumnComment("创建人")
    protected String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ColumnComment("创建时间")
    protected LocalDateTime createTime;


    /**
     * 最后修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ColumnComment("更新时间")
    protected LocalDateTime updateTime;


    @TableField(value = "update_by", fill = FieldFill.INSERT_UPDATE)
    @ColumnComment("更新人")
    protected String updateBy;


}
