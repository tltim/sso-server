package com.donger.sso.utils.entity;

import lombok.Data;

@Data
public class DropDown {
    private String label;
    private String value;
}
