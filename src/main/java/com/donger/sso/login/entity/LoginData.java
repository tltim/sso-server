package com.donger.sso.login.entity;

import lombok.Builder;
import lombok.Data;

/**
 * 登陆完成数据返回
 */
@Data
@Builder
public class LoginData {
    private String token;
    private Long userId;
    private String userName;
    private String truename;
    private String phone;
}
