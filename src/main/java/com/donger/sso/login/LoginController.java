package com.donger.sso.login;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.sso.login.entity.LoginData;
import com.donger.sso.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登陆
 *
 * @author pmc
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;


    /**
     * 登陆接口
     *
     * @return
     */
    @PostMapping("/login")
    public Result login(String username, String password) {

        LoginData loginData = loginService.loginByUsername(username, password);
        return Res.ok(loginData);
    }


    @GetMapping("/info")
    public Result info() {
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        return Res.ok(tokenInfo);
    }

    @RequestMapping("/logout")
    public Result logout(String id) {
        if (StrUtil.isEmpty(id)) {
            StpUtil.logout();
        } else {
            StpUtil.logout(id);
        }
        return Res.ok();
    }
}
