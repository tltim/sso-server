package com.donger.sso.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.util.StrUtil;
import com.donger.sso.system.entity.SysPermission;
import com.donger.sso.system.mapper.SysPermissionMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义权限验证接口扩展
 */
@Component
@AllArgsConstructor
public class StpInterfaceImpl  implements StpInterface {


    private final SysPermissionMapper sysPermissionMapper;
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<SysPermission> list = sysPermissionMapper.queryPermByUserId((Long) loginId);
        List<String> collect = list.stream().filter(sysPermission -> StrUtil.isNotBlank(sysPermission.getPerms()))
                .map(SysPermission::getPerms).collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return null;
    }
}
