package com.donger.sso.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysDictItem;
import com.donger.sso.system.mapper.SysDictItemMapper;
import org.springframework.stereotype.Service;

@Service
public class SysDictItemService extends ServiceImpl<SysDictItemMapper, SysDictItem> {
}
