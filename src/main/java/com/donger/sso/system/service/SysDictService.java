package com.donger.sso.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysDict;
import com.donger.sso.system.mapper.SysDictMapper;
import org.springframework.stereotype.Service;

@Service
public class SysDictService extends ServiceImpl<SysDictMapper, SysDict> {

}
