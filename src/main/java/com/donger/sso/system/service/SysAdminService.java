package com.donger.sso.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysAdmin;
import com.donger.sso.system.mapper.SysAdminMapper;
import org.springframework.stereotype.Service;

@Service
public class SysAdminService extends ServiceImpl<SysAdminMapper, SysAdmin> {
}
