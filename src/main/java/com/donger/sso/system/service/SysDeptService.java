package com.donger.sso.system.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysDept;
import com.donger.sso.system.entity.SysDeptRelation;
import com.donger.sso.system.mapper.SysDeptMapper;
import com.donger.sso.system.mapper.SysDeptRelationMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author pmc
 */
@Service
@AllArgsConstructor
public class SysDeptService extends ServiceImpl<SysDeptMapper, SysDept> {

    private final SysDeptRelationMapper sysDeptRelationMapper;


    /**
     * 新增部门
     * @param entity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveDept(SysDept entity) {
        super.save(entity);
        SysDeptRelation condition = new SysDeptRelation();
        condition.setDescendant(entity.getParentId());
        // 所有父节点 的祖先 增加该叶子节点

        List<SysDeptRelation> relationList = sysDeptRelationMapper.
                selectList(new LambdaQueryWrapper<SysDeptRelation>()
                        .eq(SysDeptRelation::getDescendant, entity.getParentId()))
                .stream().peek(relation -> relation.setDescendant(entity.getId())).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            sysDeptRelationMapper.saveBatch(relationList);
        }
        //自己也要维护到关系表中
        SysDeptRelation own = new SysDeptRelation();
        own.setDescendant(entity.getId());
        own.setAncestor(entity.getId());
        sysDeptRelationMapper.insert(own);

        return Boolean.TRUE;
    }

    /**
     * 更新部门
     *
     * @param sysDept 部门信息
     * @return 成功、失败
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateDeptById(SysDept sysDept) {
        //更新部门状态
        super.updateById(sysDept);
        //更新部门关系
        SysDeptRelation relation = new SysDeptRelation();
        relation.setAncestor(sysDept.getParentId());
        relation.setDescendant(sysDept.getId());
        // 删除 原有
        sysDeptRelationMapper.updateDeptRelationsdel(relation);
        // 增加新的
        sysDeptRelationMapper.updateDeptRelationsIns(relation);
        return Boolean.TRUE;
    }


    /**
     * 删除 部门
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean removeDeptById(Long id) {
        List<Long> collect = sysDeptRelationMapper.selectList(new LambdaQueryWrapper<SysDeptRelation>()
                .eq(SysDeptRelation::getAncestor, id)
        ).stream().map(SysDeptRelation::getDescendant)
                .collect(Collectors.toList());
        if (CollUtil.isNotEmpty(collect)) {
            this.removeByIds(collect);
        }
        sysDeptRelationMapper.deleteDeptRelationsById(id);
        return Boolean.TRUE;
    }
}
