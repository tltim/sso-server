package com.donger.sso.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysRolePerm;
import com.donger.sso.system.mapper.SysRolePermMapper;
import org.springframework.stereotype.Service;

/**
 * @author aeizzz
 */
@Service
public class SysRolePermService extends ServiceImpl<SysRolePermMapper, SysRolePerm> {
}
