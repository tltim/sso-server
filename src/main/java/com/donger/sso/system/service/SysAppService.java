package com.donger.sso.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysApp;
import com.donger.sso.system.mapper.SysAppMapper;
import org.springframework.stereotype.Service;

@Service
public class SysAppService extends ServiceImpl<SysAppMapper, SysApp> {
}
