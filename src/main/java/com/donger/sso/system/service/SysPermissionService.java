package com.donger.sso.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.dto.SysPermissionDTO;
import com.donger.sso.system.entity.SysPermission;
import com.donger.sso.system.mapper.SysPermissionMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysPermissionService extends ServiceImpl<SysPermissionMapper, SysPermission> {


    /**
     * 通过角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
   public List<SysPermissionDTO> findMenuByRoleId(Long roleId){

       List<SysPermission> list = baseMapper.listPermByRoleId(roleId);

       List<SysPermissionDTO> collect = list.stream().parallel().map(item -> {
           SysPermissionDTO sysPermissionDTO = new SysPermissionDTO();
           BeanUtil.copyProperties(item, sysPermissionDTO);
           return sysPermissionDTO;
       }).collect(Collectors.toList());
       recursiveBuildTree(collect,SysPermission.LEVEL_ONE_MENU);
       return collect;
    }


    /**
     * 查询用户菜单
     *
     * @param currentUserId 当前用户ID
     * @return 用户菜单列表
     */
   public List<SysPermissionDTO> getUserPermList(Long currentUserId,String code){

       List<SysPermission> list = this.baseMapper.queryPermByUserId(currentUserId);
       if(StrUtil.isNotBlank(code)){
           list = list.stream().filter(item -> item.getApplication().equals(code)).collect(Collectors.toList());
       }
       List<SysPermissionDTO> collect = list.stream().parallel().map(item -> {
           SysPermissionDTO sysPermissionDTO = new SysPermissionDTO();
           BeanUtil.copyProperties(item, sysPermissionDTO);
           return sysPermissionDTO;
       }).collect(Collectors.toList());
       recursiveBuildTree(collect,SysPermission.LEVEL_ONE_MENU);
       return collect;
   }



    /**
     * 递归建树
     *
     * @param sysMenuList 查询出的菜单数据
     * @param parentId    父节点ID
     * @return 递归后的树列表
     */
    private List<SysPermissionDTO> recursiveBuildTree(List<SysPermissionDTO> sysMenuList, Long parentId) {

        List<SysPermissionDTO> childList = sysMenuList.stream().filter(m -> m.getParentId().equals(parentId)).collect(Collectors.toList());
        if (CollUtil.isEmpty(childList)) {
            return null;
        }
        for (SysPermissionDTO sysMenu : childList) {
            sysMenu.setChildren(recursiveBuildTree(sysMenuList, sysMenu.getId()));
        }
        return childList;
    }

    public List<SysPermissionDTO> getPermList(String code) {
        List<SysPermission> list = this.baseMapper.selectList(null);
        if(StrUtil.isNotBlank(code)){
            list = list.stream().filter(item -> item.getApplication().equals(code)).collect(Collectors.toList());
        }
        List<SysPermissionDTO> collect = list.stream().parallel().map(item -> {
            SysPermissionDTO sysPermissionDTO = new SysPermissionDTO();
            BeanUtil.copyProperties(item, sysPermissionDTO);
            return sysPermissionDTO;
        }).collect(Collectors.toList());
        recursiveBuildTree(collect,SysPermission.LEVEL_ONE_MENU);
        return collect;
    }
}
