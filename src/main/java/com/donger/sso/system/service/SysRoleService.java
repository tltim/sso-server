package com.donger.sso.system.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.sso.system.entity.SysRole;
import com.donger.sso.system.entity.SysRolePerm;
import com.donger.sso.system.mapper.SysRoleMapper;
import com.donger.sso.system.mapper.SysRolePermMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author aeizzz
 */
@Service
@AllArgsConstructor
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> {
    private final SysRolePermService sysRolePermService;


    /**
     * 修改角色权限
     * @param menuList
     * @param roleId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveRoleMenu(List<Long> menuList, Long roleId) {
        sysRolePermService.removeById(roleId);
        List<SysRolePerm> list = menuList.stream().map(item -> {
            SysRolePerm temp = new SysRolePerm();
            temp.setPermsId(item);
            temp.setRoleId(roleId);
            return temp;
        }).collect(Collectors.toList());
        sysRolePermService.saveBatch(list);
        return Boolean.TRUE;
    }
}
