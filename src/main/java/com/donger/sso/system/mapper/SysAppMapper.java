package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysApp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAppMapper extends BaseMapper<SysApp> {
}
