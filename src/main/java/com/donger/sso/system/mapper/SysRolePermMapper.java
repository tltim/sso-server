package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysRolePerm;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aeizzz
 */
@Mapper
public interface SysRolePermMapper extends BaseMapper<SysRolePerm> {
}
