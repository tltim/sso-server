package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    /**
     * 根据角色获取权限列表
     * @param roleId
     * @return
     */
    @Select("SELECT sys_permission.* FROM sys_permission LEFT JOIN sys_role_perm ON sys_permission.id = sys_role_perm.perms_id " +
            " WHERE sys_role_perm.role_id = #{roleId}")
    List<SysPermission> listPermByRoleId(@Param("roleId") Long roleId);


    /**
     * 根据用户获取权限列表
     * @param currentUserId
     * @return
     */
    @Select("SELECT b.* FROM ( SELECT DISTINCT rm.perms_id FROM sys_user_role ur LEFT JOIN sys_role_perm rm ON ur.role_id = rm.role_id " +
            "  WHERE ur.user_id = #{currentUserId}\n" +
            "  ) a LEFT JOIN sys_permission b ON b.id = a.perms_id")
    List<SysPermission> queryPermByUserId(@Param("currentUserId") Long currentUserId);
 }
