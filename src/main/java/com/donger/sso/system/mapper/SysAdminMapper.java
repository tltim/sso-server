package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysAdmin;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author pmc
 */
@Mapper
public interface SysAdminMapper extends BaseMapper<SysAdmin> {
}
