package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysDept;
import com.donger.sso.system.entity.SysDeptRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author aeizzz
 */
@Mapper
public interface SysDeptRelationMapper extends BaseMapper<SysDeptRelation> {
    /**
     * 删除部门关系表数据
     *
     * @param id 部门ID
     */
    void deleteDeptRelationsById(Long id);

    /**
     * 更改部分关系表数据
     *
     * @param deptRelation 部门关系
     */
    void updateDeptRelationsdel(SysDeptRelation deptRelation);

    /**
     * 更改部分关系表数据
     *
     * @param deptRelation 部门关系
     */
    void updateDeptRelationsIns(SysDeptRelation deptRelation);

    /**
     * 查询部门节点
     *
     * @param id id
     * @return 部门节点列表
     */
    List<SysDept> queryDeptNodeList(Long id);

    /**
     * 批量保存
     *
     * @param relationLists 关系列表
     */
    void saveBatch(@Param("relationLists") List<SysDeptRelation> relationLists);
}
