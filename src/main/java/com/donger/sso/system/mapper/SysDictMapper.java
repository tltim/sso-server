package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysDict;



public interface SysDictMapper extends BaseMapper<SysDict> {
}
