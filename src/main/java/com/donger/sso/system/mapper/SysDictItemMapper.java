package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {
}
