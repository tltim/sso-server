package com.donger.sso.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.sso.system.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author pmc
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     * 查询子部门ID
     *
     * @param parentId 父部门ID
     * @return 子部门ID列表
     */
    @Select("select dept_id from sys_dept where pid = #{parentId}")
    List<Long> queryDeptIdList(@Param("parentId") Long parentId);
}
