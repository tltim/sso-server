package com.donger.sso.system.dto;

import lombok.Data;

import java.util.List;

@Data
public class SysPermissionDTO {

    private Long id;

    /**
     * 父菜单ID，一级菜单为0
     */
    private Long parentId;

    private String name;

    /**
     * 1 菜单 2 页面  3 按钮
     */
    private String type;

    private String status;


    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    private String perms;


    /**
     * 应用标识
     */
    private String application;

    private List<SysPermissionDTO> children;
}
