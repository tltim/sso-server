package com.donger.sso.system.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 接收前端参数内容
 * @author pmc
 */
@Data
public class UserDTO {

    private Long id;

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    private String username;


    /**
     * 真实姓名
     */
    @NotEmpty(message = "真实姓名不能为空")
    private String truename;


    /**
     * 状态  0：禁用   1：正常
     */
    private String status;

    private String avatar;

    @NotEmpty(message = "手机号不能为空")
    private String phone;

    @NotEmpty(message = "邮箱不能为空")
    private String email;

    /**
     * 用户类型  0 超级管理员 1 租户下管理员 2 普通用户
     */
    private String type;

    /**
     * 所属应用
     */
    private String app;

    /**
     * 所属部门
     */
    private String deptId;
}
