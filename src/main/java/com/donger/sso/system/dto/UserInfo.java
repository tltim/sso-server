package com.donger.sso.system.dto;

import lombok.Data;

/**
 * @author pmc
 * 用户信息 包含用户id 姓名，手机号，部门，等信息
 *
 */
@Data
public class UserInfo {

    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 真实姓名
     */
    private String truename;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 部门id
     */
    private String deptId;
    /**
     * 扩展信息
     */
    private String infoObj;


}
