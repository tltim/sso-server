package com.donger.sso.system.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.sso.system.entity.SysApp;
import com.donger.sso.system.service.SysAppService;
import com.donger.sso.utils.entity.BaseController;
import com.donger.sso.utils.entity.DropDown;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author pmc
 * 应用管理
 */
@RequestMapping("/app")
@RestController
@Slf4j
@AllArgsConstructor
public class SysAppController extends BaseController<SysAppService, SysApp> {

    private final SysAppService sysAppService;

    @GetMapping("/list")
    public Result list() {
        List<SysApp> list = sysAppService.list(Wrappers.<SysApp>lambdaQuery()
                .eq(SysApp::getStatus, "1"));
        return Res.ok(list);
    }

    @GetMapping("/dropdown")
    public Result dropdown() {
        List<DropDown> list = sysAppService.list(Wrappers.<SysApp>lambdaQuery()
                .eq(SysApp::getStatus, "1")).stream().map(item -> {
            DropDown dropDown = new DropDown();
            dropDown.setLabel(item.getName());
            dropDown.setValue(item.getCode());
            return dropDown;
        }).collect(Collectors.toList());
        return Res.ok(list);
    }

}
