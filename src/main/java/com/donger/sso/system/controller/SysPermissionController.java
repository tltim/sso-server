package com.donger.sso.system.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.common.core.utils.BizException;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.sso.system.dto.SysPermissionDTO;
import com.donger.sso.system.entity.SysPermission;
import com.donger.sso.system.service.SysPermissionService;
import com.donger.sso.utils.AuthUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * 权限
 * @author aeizzz
 */
@RequestMapping("/perm")
@RestController
@Slf4j
@AllArgsConstructor
public class SysPermissionController {


    private final SysPermissionService sysPermissionService;


    /**
     * 获取菜单列表
     *
     * @param code
     * @return
     */
    @GetMapping("/nav")
    public Result nav(String code) {
        Long userId = AuthUtils.getUserId();
        List<SysPermissionDTO> userMenuNoButton = sysPermissionService.getUserPermList(userId, code);
        return Res.ok(userMenuNoButton);
    }


    @GetMapping("list")
    public Result list(String code){
        List<SysPermissionDTO> userMenuNoButton = sysPermissionService.getPermList(code);
        return Res.ok(userMenuNoButton);
    }

    @GetMapping("/select")
    public Result select(String code){
        Long userId = AuthUtils.getUserId();
        List<SysPermissionDTO> userMenuNoButton = sysPermissionService.getUserPermList(userId, code);

        SysPermissionDTO root = new SysPermissionDTO();
        root.setId(0L);
        root.setName("顶级菜单");
        root.setChildren(userMenuNoButton);
        List<SysPermissionDTO> roots = new ArrayList<>();
        roots.add(root);
        return Res.ok(roots);
    }


    /**
     * 菜单信息
     */
    @GetMapping("/info/{menuId}")
    @ApiOperation(value = "权限详情")
    public Result<SysPermissionDTO> info(@ApiParam(value = "权限ID") @PathVariable("menuId") Long menuId) {

        SysPermission permission = sysPermissionService.getById(menuId);

        SysPermissionDTO sysPermissionDTO = new SysPermissionDTO();
        BeanUtil.copyProperties(permission, sysPermissionDTO);

        return Res.ok(sysPermissionDTO);
    }


    /**
     * 保存
     */
    @PostMapping("/save")
    @SaCheckPermission("sys:permission:add")
    @ApiOperation(value = "新增权限,具备权限:sys:permission:add")
    public Result<Boolean> save(@RequestBody @Valid SysPermission menu) {
        if (menu.getParentId() == null) {
            menu.setParentId(0L);
        }
        return Res.ok(sysPermissionService.save(menu));
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @SaCheckPermission("sys:permission:update")
    @ApiOperation(value = "更新权限,具备权限:sys:permission:update")
    public Result<Boolean> update(@RequestBody SysPermission menu) {
        //数据校验
        verifyForm(menu);
        return Res.ok(sysPermissionService.updateById(menu));
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete/{permId}")
    @ApiOperation("根据权限ID删除权限,具备权限:sys:permission:delete")
    @SaCheckPermission("sys:permission:delete")
    public Result<Boolean> delete(@PathVariable("permId") Long permId) {
        //判断是否有子菜单或按钮
        int count = sysPermissionService.count(Wrappers.<SysPermission>lambdaQuery().eq(SysPermission::getParentId, permId));
        if (count > 0) {
            return Res.error("请先删除子菜单或按钮");
        }
        sysPermissionService.removeById(permId);
        return Res.ok();
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysPermission menu) {
        if (StrUtil.isBlank(menu.getName())) {
            throw new BizException("菜单名称不能为空");
        }

        if (menu.getParentId() == null) {
            throw new BizException("上级菜单不能为空");
        }

    }
}
