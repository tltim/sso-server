package com.donger.sso.system.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.sso.system.entity.SysDept;
import com.donger.sso.system.service.SysDeptService;
import com.donger.sso.utils.entity.BaseController;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理
 * @author aeizzz
 */
@RequestMapping("/dept")
@RestController
public class SysDeptController extends BaseController<SysDeptService, SysDept> {

    /**
     * 查询所有部门
     * @return
     */
    @GetMapping("tree")
    public Result<List<SysDept>> list() {
        List<SysDept> sysDeptList = baseService.list();
        return Res.ok(recursiveBuildTree(sysDeptList, SysDept.LEVEL_ONE_DEPT));
    }


    /**
     * 选择部门(添加、修改菜单)
     */
    @GetMapping("/select")
    public Result select() {
        List<SysDept> deptList = baseService.list();
        SysDept root = new SysDept();
        root.setId(0L);
        root.setName("一级部门");
        root.setParentId(-1L);
        deptList.add(root);
        return Res.ok(recursiveBuildTree(deptList, -1L));
    }


    /**
     *  修改时需要查询的树类型
     * @return
     */
    @GetMapping("/edit/select")
    public Result editTree(Long deptId){

        List<SysDept> deptList = baseService.list(Wrappers.<SysDept>lambdaQuery().notIn(SysDept::getId,deptId));
        SysDept root = new SysDept();
        root.setId(0L);
        root.setName("一级部门");
        root.setParentId(-1L);
        deptList.add(root);
        return Res.ok(recursiveBuildTree(deptList, -1L));
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @Override
    public Result<Boolean> save(@RequestBody SysDept dept) {
        return Res.ok(baseService.saveDept(dept));
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody SysDept dept) {
        return Res.ok(baseService.updateDeptById(dept));
    }


    /**
     * 删除
     */
    @DeleteMapping("/delete/{deptId}")
    @Override
    public Result delete(@PathVariable("deptId") Long deptId) {
        //判断是否有子部门
        int count = baseService.count(Wrappers.<SysDept>lambdaQuery().eq(SysDept::getParentId, deptId));
        if (count > 0) {
            return Res.error("请先删除子部门");
        }
        return Res.ok(baseService.removeDeptById(deptId));
    }


    /**
     * 递归建树
     * 不包含根节点
     * @param sysDeptList 查询出的部门数据
     * @param parentId    父节点ID
     * @return 递归后的树列表
     */
    private List<SysDept> recursiveBuildTree(List<SysDept> sysDeptList, Long parentId) {
        List<SysDept> childList = sysDeptList.stream().filter(m -> m.getParentId().equals(parentId)).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(childList)) {
            return null;
        }
        for (SysDept sysDept : childList) {
            sysDept.setChildren(recursiveBuildTree(sysDeptList, sysDept.getId()));
        }
        return childList;
    }


    /**
     * 包含根节点
     * @param sysDeptList
     * @param parentId
     * @return
     */
    private List<SysDept> recursiveBuildTreeRoot(List<SysDept> sysDeptList, Long parentId) {
        List<SysDept> childList = sysDeptList.stream().filter(m -> m.getId().equals(parentId)).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(childList)) {
            return null;
        }
        for (SysDept sysDept : childList) {
            sysDept.setChildren(recursiveBuildTree(sysDeptList, sysDept.getId()));
        }
        return childList;
    }
}
