package com.donger.sso.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.sso.system.dto.SysPermissionDTO;
import com.donger.sso.system.entity.SysPermission;
import com.donger.sso.system.entity.SysRole;
import com.donger.sso.system.service.SysPermissionService;
import com.donger.sso.system.service.SysRoleService;
import com.donger.sso.utils.entity.BaseController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色管理
 */
@RequestMapping("/role")
@RestController
@Slf4j
@AllArgsConstructor
public class SysRoleController extends BaseController<SysRoleService,SysRole> {

    private final SysPermissionService sysPermissionService;



    /**
     * 查询角色所对应的权限
     *
     * @param roleId
     * @return
     */
    @GetMapping("/info/menu/{roleId}")
    @ApiOperation(value = "查询角色对应权限信息")
    public Result<List<Long>> menuList(@ApiParam(name = "角色id")@PathVariable("roleId") Long roleId) {
        List<Long> collect = sysPermissionService.findMenuByRoleId(roleId)
                .stream().map(SysPermissionDTO::getId).collect(Collectors.toList());
        return Res.ok(collect);
    }


    /**
     * 删除原来权限  增加新菜单
     *
     * @param menuList
     * @return
     */
    @PostMapping("/info/menu/{roleId}")
    @ApiOperation(value = "给角色增加对应的权限")
    public Result saveMenu(@ApiParam(name = "权限列表") @RequestBody List<Long> menuList,
                           @ApiParam(name = "角色id")@PathVariable Long roleId) {
        baseService.saveRoleMenu(menuList, roleId);
        return Res.ok("保存成功");
    }



}
