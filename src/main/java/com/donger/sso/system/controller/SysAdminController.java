package com.donger.sso.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.sso.system.entity.SysAdmin;
import com.donger.sso.system.service.SysAdminService;
import com.donger.sso.utils.entity.BaseController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 管理员
 *
 * @author pmc
 */
@RestController
@RequestMapping("/admin")
@AllArgsConstructor
public class SysAdminController extends BaseController<SysAdminService,SysAdmin> {


}
