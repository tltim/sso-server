package com.donger.sso.system.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.sso.system.dto.UserDTO;
import com.donger.sso.system.dto.UserInfo;
import com.donger.sso.system.entity.SysUser;
import com.donger.sso.system.service.SysUserService;
import com.donger.sso.utils.entity.BaseController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户管理
 *
 * @author pmc
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class SysUserController extends BaseController<SysUserService, SysUser> {


    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/userinfo")
    public Result<UserInfo> userinfo(Long userId) {
        log.info("当前登陆用户 -》 {}", StpUtil.getLoginId());
        SysUser user = sysUserService.getById(userId);
        UserInfo userInfo = new UserInfo();
        BeanUtil.copyProperties(user, userInfo);
        return Res.ok(userInfo);
    }

    /**
     * 保存用户
     */
    @PostMapping("/saveUser")
    @ApiOperation(value = "新增用户")
    public Result<Boolean> save(@RequestBody @Valid UserDTO user) {
        sysUserService.saveUser(user);
        return Res.ok();
    }

    /**
     * 修改用户
     */
    @PostMapping("/update")
    @ApiOperation(value = "更新用户")
    public Result<Boolean> update(@RequestBody @Valid UserDTO user) {
        if (user.getId() == null) {
            return Res.ok("用户id不能为空");
        }
        sysUserService.updateUser(user);
        return Res.ok();
    }

}
