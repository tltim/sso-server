package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户与角色对应关系
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@Accessors(chain = true)
@TableComment("用户角色关联表")
@TableName
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private Long id;

    /**
     * 用户ID
     */
    @TableField
    @ColumnComment("用户ID")
    private Long userId;

    /**
     * 角色ID
     */
    @TableField
    @ColumnComment("角色ID")
    private Long roleId;


}
