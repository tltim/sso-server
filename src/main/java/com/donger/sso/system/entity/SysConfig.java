package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * <p>
 * 系统配置信息表
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName
@TableComment("系统配置信息表")
public class SysConfig extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @TableField
    @ColumnComment("key")
    private String paramKey;

    /**
     * value
     */
    @TableField
    @ColumnComment("value")
    private String paramValue;

    /**
     * 状态   0：隐藏   1：显示
     */
    @TableField
    @ColumnComment("状态   0：隐藏   1：显示")
    private Integer status;

    /**
     * 备注
     */
    @TableField
    @ColumnComment("备注")
    private String remark;
}
