package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.donger.sso.utils.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import lombok.Data;

import java.io.Serializable;

/**
 * 管理员表  主要为sso 管理员
 */
@Data
public class SysAdmin extends BaseEntity implements Serializable {

    /**
     * 用户名
     */

    @TableField
    @ColumnComment("用户名")
    private String username;
    /**
     * 真实姓名
     */
    @TableField
    @ColumnComment("真实姓名")
    private String truename;
    /**
     * 密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableField
    @ColumnComment("密码")
    private String password;
    /**
     * 状态 N 进行 Y 启用
     */
    @TableField
    @ColumnComment("状态 N 进行 Y 启用")
    private String status;
    /**
     * 手机号
     */
    @TableField
    @ColumnComment("手机号")
    private String phone;
    /**
     * 头像
     */
    @TableField
    @ColumnComment("头像")
    private String avatar;
}
