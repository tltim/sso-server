package com.donger.sso.system.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 部门关系表
 *
 * @author aeizzz
 */
@Data
@Accessors(chain = true)
@TableComment("部门关系表")
@TableName
public class SysDeptRelation implements Serializable {

    /**
     * 祖先
     */
    @TableField
    @ColumnComment("祖先")
    @NotNull
    private Long ancestor;

    /**
     * 后代
     */
    @TableField
    @ColumnComment("后代")
    @NotNull
    private Long descendant;
}
