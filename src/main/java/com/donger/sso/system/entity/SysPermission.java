package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.donger.sso.utils.entity.BaseTree;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 权限管理
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@Accessors(chain = true)
@TableName
@TableComment("权限管理")
public class SysPermission extends BaseTree implements Serializable {

    public static final long LEVEL_ONE_MENU = 0L;
    public static final long SENSITIVE_MENU_ID = 31L;
    private static final long serialVersionUID = 1L;


    @TableField
    @ColumnComment("权限名称")
    private String name;


    /**
     * 1 菜单 2 页面  3 按钮
     */
    @TableField
    @ColumnComment("权限类型")
    private String type;

    @TableField
    @ColumnComment("权限状态")
    private String status;

    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    @TableField
    @ColumnComment("授权")
    private String perms;

    /**
     * 应用标识
     */
    @TableField
    @ColumnComment("应用标识")
    private String application;

}
