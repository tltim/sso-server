package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@Accessors(chain = true)
@TableName
@TableComment("客户端用户")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    @ColumnComment("id")
    private Long id;

    /**
     * 用户名
     */
    @TableField
    @ColumnComment("用户名")
    private String username;


    /**
     * 真实姓名
     */
    @TableField
    @ColumnComment("真实姓名")
    private String truename;

    /**
     * 状态  N：禁用   Y：正常
     */
    @TableField
    @ColumnComment("状态")
    @DefaultValue("Y")
    private String status;

    @TableField
    @ColumnComment("头像")
    private String avatar;

    @TableField
    @ColumnComment("手机号")
    private String phone;

    /**
     * 密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableField
    @ColumnComment("密码")
    private String password;


    @TableField
    @ColumnComment("最后一次登录ip")
    private String lastIp;


    @TableField
    @ColumnComment("注册时间")
    private LocalDateTime registTime;


    @TableField
    @ColumnComment("最后一次登录时间")
    private LocalDateTime lastLoginTime;


    @TableField
    @ColumnComment("登录次数")
    private Long loginNum;


    @TableField
    @ColumnComment("邮箱")
    private String email;

    /**
     * 用户类型  0 超级管理员 1 租户下管理员 2 普通用户
     */
    @TableField
    @ColumnComment("用户类型")
    @DefaultValue("2")
    private String type;


    @TableField
    @ColumnComment("所属应用")
    private String app;

    @TableField
    @ColumnComment("部门id")
    private Long deptId;


    @TableField
    @ColumnComment("扩展属性")
    private String extended;

}
