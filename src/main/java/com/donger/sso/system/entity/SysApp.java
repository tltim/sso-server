package com.donger.sso.system.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 应用管理
 */

@TableName
@Data
@TableComment("应用管理")
public class SysApp extends BaseEntity {

    @TableField
    @ColumnComment("应用编码")
    @IsNotNull
    private String code;


    @TableField
    @ColumnComment("应用名称")
    @IsNotNull
    private String name;


    @TableField
    @ColumnComment("别名")
    @IsNotNull
    private String alias;


    @TableField
    @ColumnComment("地址")
    @IsNotNull
    private String url;

    /**
     * 0 禁用 1 启用
     */
    @TableField
    @ColumnComment("应用状态")
    @IsNotNull
    @DefaultValue("1")
    private String status;





}
