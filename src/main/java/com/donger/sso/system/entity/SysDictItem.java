package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 7326
 */
@Data
@Accessors(chain = true)
@TableComment("字典项")
@TableName
public class SysDictItem extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableField
    @ColumnComment("字典类id")
    private Long dictId;
    @TableField
    @ColumnComment("字典项名称")
    private String itemText;
    @TableField
    @ColumnComment("字典项编码")
    private String itemCode;
    @TableField
    @ColumnComment("备注")
    private String remark;
}
