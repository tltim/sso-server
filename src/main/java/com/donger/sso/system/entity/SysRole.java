package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@Accessors(chain = true)
@TableName
@TableComment("角色管理")
public class SysRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @TableField
    @ColumnComment("角色名称")
    private String roleName;
    /**
     * 角色标识
     */
    @TableField
    @ColumnComment("角色标识")
    private String roleCode;

    /**
     * 数据权限类型
     */
    @TableField
    @ColumnComment("数据权限类型")
    private String dsType;

    /**
     * 数据权限范围
     */
    @TableField
    @ColumnComment("数据权限范围")
    private String dsScope;


    /** 是否系统内置 Y 内置 N 不是  内置 不能修改*/
    @TableField
    @ColumnComment("是否系统内置 Y 内置 N 不是")
    private String isSystem;


}
