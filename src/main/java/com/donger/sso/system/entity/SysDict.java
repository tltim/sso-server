package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典类
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName
@TableComment("字典类")
public class SysDict extends BaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 字典名
     */
    @TableField
    @ColumnComment("字典名")
    private String name;


    /**
     * 字典类型
     */
    @TableField
    @ColumnComment("字典类型")
    private String type;

    /**
     * 备注
     */
    @TableField
    @ColumnComment("备注")
    private String remark;


    @TableField
    @ColumnComment("排序号")
    private Integer orderNum;

}
