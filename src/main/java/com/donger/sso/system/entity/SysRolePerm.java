package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 角色与菜单对应关系
 * </p>
 *
 * @author xyx
 * @since 2019-01-12
 */
@Data
@Accessors(chain = true)
@TableName
@TableComment("角色权限关联表")
public class SysRolePerm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    @ColumnComment("ID")
    private Long id;

    /**
     * 角色ID
     */
    @TableField
    @ColumnComment("角色ID")
    private Long roleId;

    /**
     * 菜单ID
     */
    @TableField
    @ColumnComment("权限ID")
    private Long permsId;


}
