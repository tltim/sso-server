package com.donger.sso.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.sso.utils.entity.BaseEntity;
import com.donger.sso.utils.entity.BaseTree;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 部门信息
 * @author pmc
 */
@Data
@TableName
@TableComment("部门信息")
public class SysDept extends BaseTree implements Serializable {

    public static final long LEVEL_ONE_DEPT = 0L;


    /**
     * 部门名称
     */
    @TableField
    @ColumnComment("部门名称")
    private String name;

    /**
     * 排序
     */
    @TableField
    @ColumnComment("排序")
    @DefaultValue("0")
    private Integer orderNum;


    @TableField(exist = false)
    private List<SysDept> children;
}
