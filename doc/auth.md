

## 授权码模式
```
oauth2/authorize
    ?response_type=code
    &client_id={value}
    &redirect_uri={value}
    &scope={value}
    $state={value}
    
 
 oauth2/authorize?response_type=code&client_id=1001&redirect_uri=http://sa-token.dev33.cn/&scope=userinfo
```


## 隐藏式（Implicit）
```
oauth2/authorize
    ?response_type=token
    &client_id={value}
    &redirect_uri={value}
    &scope={value}
    $state={value}
    
    
 oauth2/authorize?response_type=token&client_id=1001&redirect_uri=http://sa-token.dev33.cn/&scope=userinfo
```


## 凭证式（Client Credentials）
```
oauth2/client_token
    ?grant_type=client_credentials
    &client_id={value}
    &client_secret={value}

 oauth2/client_token?response_type=client_credentials&client_id=1001&client_secret=aaaa-bbbb-cccc-dddd-eeee
```


## 密码式（Password）
```
oauth2/token
    ?grant_type=password
    &client_id={value}
    &username={value}
    &password={value}
    
 oauth2/token?response_type=password&client_id=1001&username=sa&password=123456
```