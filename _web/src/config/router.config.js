// eslint-disable-next-line
import { BasicLayout, BlankLayout, PageView, UserLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

/**
 * 菜单所需要的字段
 * @type {*[]}
 * path
 * meta : {
 *   title: '首页',
 *   keepAlive: true,
 *   icon: bxAnaalyse
 * }
 * hidden: 可以在菜单中不展示这个路由，包括子路由
 * hideChildrenInMenu: 隐藏不需要在菜单中展示的子路由
 * children: [
 *
 * ]
 *
 */
export const asyncRouterMap = [

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/analysis',
        hidden: true,
        component: PageView,
        meta: { title: '仪表盘', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'analysis',
            name: 'Analysis',
            component: () => import('@/views/dashboard/Analysis'),
            meta: { title: '工作台', keepAlive: false }
          }
        ]
      },
      {
        path: '/upms',
        name: 'upms',
        component: PageView,
        meta: { title: '权限管理', keepAlive: true, icon: 'safety-certificate' },
        children: [
          {
            path: 'admin',
            name: 'admin',
            component: () => import('@/views/modules/sys/admin/adminList'),
            meta: { title: '管理员列表', keepAlive: true }
          },
          {
            path: 'apps',
            name: 'apps',
            component: () => import('@/views/modules/sys/apps'),
            meta: { title: '应用管理', keepAlive: true }
          },
          {
            path: 'perm',
            name: 'perm',
            component: () => import('@/views/modules/sys/perm'),
            meta: { title: '权限管理', keepAlive: true }
          },
          {
            path: 'role',
            name: 'role',
            component: () => import('@/views/modules/sys/role'),
            meta: { title: '角色管理', keepAlive: true}
          },
          {
            path: 'dept',
            name: 'dept',
            component: () => import('@/views/modules/sys/dept/dept'),
            meta: { title: '部门管理', keepAlive: true}
          }
        ]
      },
      {
        path: '/user',
        name: 'user',
        component: PageView,
        meta: { title: '用户管理', keepAlive: true, icon: 'safety-certificate' },
        children: [
          {
            path: 'user-list',
            name: 'user-list',
            component: () => import('@/views/modules/sys/user/userList'),
            meta: { title: '用户列表', keepAlive: true }
          },
        ]
      },
      // account
      {
        path: '/account',
        component: PageView,
        redirect: '/account/center',
        name: 'account',
        hideChildrenInMenu: true,
        hidden: true,
        meta: { title: '个人页', icon: 'user', keepAlive: true },
        children: [
          {
            path: '/account/settings',
            name: 'settings',
            component: () => import('@/views/account/settings/Index'),
            meta: { title: '个人设置', hideHeader: true },
            redirect: '/account/settings/base',
            children: [
              {
                path: '/account/settings/base',
                name: 'BaseSettings',
                component: () => import('@/views/account/settings/BaseSetting'),
                meta: { title: '基本设置', hidden: true }
              },
              {
                path: '/account/settings/custom',
                name: 'CustomSettings',
                component: () => import('@/views/account/settings/Custom'),
                meta: { title: '个性化设置', hidden: true, keepAlive: true }
              },
              {
                path: '/account/settings/Binding',
                name: 'BindingSettings',
                component: () => import('@/views/account/settings/Binding'),
                meta: { title: '账户绑定', hidden: true, keepAlive: true }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/page/login')
      }
    ]
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
