import Vue from 'vue'
import { login, getInfo, logout, loginBySocial, loginByPhone } from '@/api/login'
import { ACCESS_TOKEN,ALL_APPS_MENU } from '@/store/mutation-types'
import { welcome, encryption } from '@/utils/util'
import router from '../../router'
import store from '../index'
const user = {
  state: {
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {},
    apps: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_APPS: (state, apps) => {
      state.apps = apps
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
   //   const user = encryption({
   //    data: userInfo,
   //    key: 'bootbootbootboot',
   //    param: ['password']
   //  })
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
          // 存储token 设置过期时间
          const result = response
          Vue.ls.set(ACCESS_TOKEN, result.data.token)
          commit('SET_TOKEN', result.data.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 根据OpenId登录
    LoginBySocial ({ commit }, param) {
      return new Promise((resolve, reject) => {
        loginBySocial(param.state, param.code).then(response => {
          // 存储token 设置过期时间
          const result = response
          console.log(result,'result')
          Vue.ls.set(ACCESS_TOKEN, result.data.token)
          commit('SET_TOKEN', result.data.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    loginByPhone ({ commit }, param) {
      return new Promise((resolve, reject) => {
        loginByPhone(param.mobile, param.code).then(response => {
          // 存储token 设置过期时间
          const result = response
          console.log(result,'result')
          Vue.ls.set(ACCESS_TOKEN, result.data.token)
          commit('SET_TOKEN', result.data.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取用户信息
    GetInfo ({ commit }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          const result = response.data
          // commit('SET_ROLES', role)
          // commit('SET_INFO', )
          commit('SET_NAME', { name: "管理员", welcome: welcome() })
          // commit('SET_AVATAR', result.sysUser.avatar)
          // commit('SET_APPS',result.apps)
          resolve(result)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout ({ commit, state }) {
      return new Promise((resolve) => {
        logout(state.token).then(() => {
          commit('SET_ROLES', [])
          Vue.ls.remove(ACCESS_TOKEN)
          Vue.ls.remove('TENANT_ID')
          commit('SET_TOKEN', '')
          resolve()
        }).catch(() => {
          commit('SET_ROLES', [])
          Vue.ls.remove(ACCESS_TOKEN)
          Vue.ls.remove('TENANT_ID')
          commit('SET_TOKEN', '')
          resolve()
        })
      })
    },
    // 推出删除token
    DelToken ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('SET_ROLES', [])
        Vue.ls.remove(ACCESS_TOKEN)
        commit('SET_TOKEN', '')
        Vue.ls.remove('TENANT_ID')
        resolve()
      })
    },
    // 切换应用菜单
    MenuChange ({ commit }, application) {
      return new Promise((resolve) => {
        console.log(application,'app')
        const apps = { 'code': '', 'name': '', 'active': '', 'menu': '' }
        apps.active = true
        const all_app_menu = Vue.ls.get(ALL_APPS_MENU)
        // eslint-disable-next-line camelcase
        const new_false_all_app_menu = []
        // 先去除所有默认的，以为此时切换的即将成为前端缓存默认的应用
        all_app_menu.forEach(item => {
          if (item.active) {
            item.active = false
          }
          new_false_all_app_menu.push(item)
        })
        // 此时缓存中全部都是不默认的应用
        Vue.ls.set(ALL_APPS_MENU, new_false_all_app_menu)
        apps.name = application.name
        apps.code = application.code
        const applocationR = []
        applocationR.push(apps)
        Vue.ls.set(ALL_APPS_MENU, applocationR)
        store.dispatch('GenerateRoutes', application.code).then(() => {
          router.addRoutes(store.getters.addRouters)
        })
        resolve()
      })
    }

  }
}

export default user
