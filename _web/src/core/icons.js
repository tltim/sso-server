/**
 * Custom icon list
 * All icons are loaded here for easy management
 * @see https://vue.ant.design/components/icon/#Custom-Font-Icon
 *
 * 自定义图标加载表
 * 所有图标均从这里加载，方便管理
 */
import bxAnaalyse from '@/assets/icons/bx-analyse.svg?inline' // path to your '*.svg?inline' file.
import wechat from '@/assets/icons/wechat.svg?inline'
import qq from '@/assets/icons/qq.svg?inline'
import dingding from '@/assets/icons/dingding.svg?inline'
import gitee from '@/assets/icons/gitee.svg?inline'

export { bxAnaalyse, wechat, qq, dingding, gitee }
