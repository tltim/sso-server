import Vue from 'vue'
import VueStorage from 'vue-ls'
import config from '@/config/defaultSettings'
// base library
import Antd from 'ant-design-vue'
import VueCropper from 'vue-cropper'
import 'ant-design-vue/dist/antd.less'
import VTable from '@/components/VxeTable'
// 动态表单
import KFormDesign from 'k-form-design'
import 'k-form-design/lib/k-form-design.css'

import BizCom from '@/components/BaseComp'
// ext library
import VueClipboard from 'vue-clipboard2'
// import '@/components/use'
VueClipboard.config.autoSetContainer = true

Vue.use(KFormDesign)
Vue.use(Antd)
Vue.use(VTable)
Vue.use(BizCom)
Vue.use(VueStorage, config.storageOptions)
Vue.use(VueClipboard)
Vue.use(VueCropper)
