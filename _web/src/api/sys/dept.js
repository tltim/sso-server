import { axios } from '@/utils/request'

export function fetchList () {
  return axios({
    url: '/dept/tree',
    msthod: 'GET'
  })
}

export function fetchTree () {
  return axios({
    url: '/dept/select',
    method: 'GET'
  })
}

export function getObj (id) {
  return axios({
    url: '/dept/info/' + id,
    method: 'GET'
  })
}

export function addObj (obj) {
  return axios({
    url: '/dept/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/dept/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: '/dept/delete/' + id,
    method: 'DELETE'
  })
}

/**
 * 查询用户所在部门下的部门 tree
 * 我的部门中使用
 * @returns {AxiosPromise}
 */
export function queryDeptNode () {
  return axios({
    url: '/dept/dept/deptUserList',
    method: 'GET'
  })
}

export function EditTree (deptid) {
  return axios({
    url: '/dept/edit/select',
    method: 'get',
    params: {
      deptId: deptid
    }
  })
}
