import { axios } from '@/utils/request'

const prefix = 'sys/label'

export function fetchList (params, type) {
  return axios({
    url: `/${prefix}/${type}/page`,
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: `/${prefix}/update`,
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del/${id}`,
    method: 'DELETE'
  })
}

export function dropdown (type) {
  return axios({
    url: `/${prefix}/${type}/dropsown/`,
    method: 'GET'
  })
}
