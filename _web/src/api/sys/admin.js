import { axios } from '@/utils/request'


/**
 * 高级查询
 * @param id
 * @return
 */
export function PageList (params) {
  return axios({
    url: `/admin/page`,
    method: 'GET',
    params: {
      ...params
    }
  })
}

export function getObj (id) {
  return axios({
    url: '/admin/info/' + id,
    method: 'get'
  })
}

export function addObj (obj) {
  return axios({
    url: '/admin/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/admin/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: '/admin/delete/' + id,
    method: 'GET'
  })
}