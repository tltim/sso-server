import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/log/page',
    method: 'get',
    params: params
  })
}

export function getObj (id) {
  return axios({
    url: `/sys/log/info/${id}`,
    method: 'get'
  })
}

export function delObj (id) {
  return axios({
    url: `/sys/log/delete/${id}`,
    method: 'delete'
  })
}
