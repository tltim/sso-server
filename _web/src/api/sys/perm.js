import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/perm/list',
    method: 'GET',
    params
  })
}


export function getObj (id) {
  return axios({
    url: '/perm/info/' + id,
    method: 'GET'
  })
}

export function addObj (obj) {
  return axios({
    url: '/perm/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/perm/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: '/perm/delete/' + id,
    method: 'DELETE'
  })
}
