import { axios } from '@/utils/request'

const prefix = 'app'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info`,
    method: 'GET',
    params: {
      id
    }
  })
}

export function putObj (obj) {
  return axios({
    url: `/${prefix}/update`,
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del`,
    method: 'DELETE',
    params: {
      id
    }
  })
}

export function list(){
  return axios({
    url: `/${prefix}/list`,
    method: 'GET'
  })
}