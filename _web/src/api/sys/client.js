import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/client/page',
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: '/sys/client/save',
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/sys/client/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: '/sys/client/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/sys/client/delete/${id}`,
    method: 'DELETE'
  })
}
