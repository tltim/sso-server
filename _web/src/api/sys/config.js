import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/config/page',
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: '/sys/config/save',
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/sys/config/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: '/sys/config/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/sys/config/del/${id}`,
    method: 'DELETE'
  })
}
