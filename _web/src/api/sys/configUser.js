import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/configUser/page',
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: '/sys/configUser/save',
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/sys/configUser/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: '/sys/configUser/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/sys/configUser/del/${id}`,
    method: 'DELETE'
  })
}
