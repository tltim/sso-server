import { axios } from '@/utils/request'

export function roleDropDown () {
  return axios({
    url: '/sys/role/dropdown',
    method: 'GET'
  })
}

export function fetchList (params) {
  return axios({
    url: '/role/page',
    method: 'GET',
    params: params
  })
}

export function list () {
  return axios({
    url: '/role/list',
    method: 'GET'
  })
}

export function getObj (id) {
  return axios({
    url: `/role/info/${id}`,
    method: 'GET'
  })
}

export function addObj (obj) {
  return axios({
    url: '/role/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/role/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/role/delete/${id}`,
    method: 'DELETE'
  })
}

export function roleMenuIds (roleId) {
  return axios({
    url: `/role/info/menu/${roleId}`,
    method: 'GET'
  })
}

export function saveRoleMenu (menuList, roleId) {
  return axios({
    url: `/role/info/menu/${roleId}`,
    method: 'POST',
    data: menuList
  })
}
