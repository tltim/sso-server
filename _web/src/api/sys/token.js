import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/token/page',
    method: 'POST',
    data: params
  })
}

export function delObj (id) {
  return axios({
    url: `/token/${id}`,
    method: 'DELETE'
  })
}
