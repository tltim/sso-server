import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/dict/page',
    method: 'GET',
    params: params
  })
}

export function fetchTreeList (params) {
  return axios({
    url: '/sys/dict/list',
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: '/sys/dict/save',
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: '/sys/dict/info/' + id,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: '/sys/dict/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: '/sys/dict/delete/' + id,
    method: 'DELETE'
  })
}

export function dictItemList (params) {
  return axios({
    url: '/sys/dict/item/list',
    method: 'GET',
    params: params
  })
}

export function addItemObj (obj) {
  return axios({
    url: '/sys/dict/item/save',
    method: 'POST',
    data: obj
  })
}

export function getItemObj (id) {
  return axios({
    url: '/sys/dict/item/info/' + id,
    method: 'GET'
  })
}

export function putItemObj (obj) {
  return axios({
    url: '/sys/dict/item/update',
    method: 'POST',
    data: obj
  })
}

export function delItemObj (id) {
  return axios({
    url: '/sys/dict/item/delete/' + id,
    method: 'DELETE'
  })
}
export function delItemList (ids) {
  return axios({
    url: '/sys/dict/item/deleteList',
    method: 'DELETE',
    params: {
      ids: ids
    }
  })
}
export function getDictByType (type) {
  return axios({
    url: `/sys/dict/type/${type}`
  })
}
