import { axios } from '@/utils/request'

export function fetchList (params) {
  return axios({
    url: '/sys/social/page',
    method: 'GET',
    params: params
  })
}

export function getObj (id) {
  return axios({
    url: `/sys/social/info/${id}`,
    method: 'GET'
  })
}

export function addObj (obj) {
  return axios({
    url: '/sys/social/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/sys/social/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/sys/social/del/${id}`,
    method: 'DELETE'
  })
}
