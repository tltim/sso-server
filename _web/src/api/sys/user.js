import { axios } from '@/utils/request'


/**
 * 高级查询
 * @param id
 * @return
 */
export function PageList (params) {
  return axios({
    url: `/user/page`,
    method: 'GET',
    params: {
      ...params
    }
  })
}

export function getObj (id) {
  return axios({
    url: '/user/info/' + id,
    method: 'get'
  })
}

export function addObj (obj) {
  return axios({
    url: '/user/save',
    method: 'POST',
    data: obj
  })
}

export function putObj (obj) {
  return axios({
    url: '/user/update',
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: '/user/delete/' + id,
    method: 'GET'
  })
}