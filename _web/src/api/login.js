import { axios } from '@/utils/request'
import qs from 'qs'
/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
export function login (parameter) {
  const data = { ...parameter }
  return axios({
    url: '/login',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function getInfo () {
  return axios({
    url: '/info',
    method: 'get'
  })
}

export function logout () {
  return axios({
    url: '/token/logout',
    method: 'DELETE'
  })
}

export function menu () {
  return axios({
    url: '/sys/menu/nav',
    method: 'GET'
  })
}

export function sendSmsCode (mobile) {
  return axios({
    url: `/captcha/${mobile}`,
    method: 'GET'
  })
}

export function updateUserAvatar (data) {
  return axios({
    url: '/sys/user/updateAvatar',
    method: 'PUT',
    data: qs.stringify(data)
  })
}

export function bindUser (data) {
  return axios({
    url: '/sys/social/bind',
    method: 'POST',
    params: data
  })
}

export function loginBySocial (state, code) {
  // eslint-disable-next-line camelcase
  const grant_type = 'mobile'
  return axios({
    url: '/mobile/token/social',
    method: 'POST',
    headers: {
      'isToken': false,
      'Authorization': 'Basic Y2hpbGxheC1ib290OmNoaWxsYXgtYm9vdC1zZWNyZXQ='
    },
    data: qs.stringify({ mobile: state + '@' + code, grant_type })
  })
}

export function loginByPhone (mobile, code) {
  // eslint-disable-next-line camelcase
  const grant_type = 'mobile'
  return axios({
    url: '/mobile/token/sms',
    method: 'POST',
    headers: {
      'isToken': false,
      'Authorization': 'Basic Y2hpbGxheC1ib290OmNoaWxsYXgtYm9vdC1zZWNyZXQ='
    },
    data: qs.stringify({ mobile: 'SMS@' + mobile, grant_type, code })
  })
}

export function menuNav (code) {
  return axios({
    url: '/sys/permission/nav',
    method: 'GET',
    params: {
      code
    }
  })
}
