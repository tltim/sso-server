export const DataScopeTypeList = [
  {
    label: '全部',
    value: '0'
  },
  {
    label: '自定义',
    value: '1'
  },
  {
    label: '本级',
    value: '3'
  },
  {
    label: '本级及子级',
    value: '2'
  }
]

export const statusTypeList = [
  {
    label: '禁用',
    value: 'N'
  },
  {
    label: '正常',
    value: 'Y'
  }
]



export const authorizedGrantTypeList = [
  {
    label: 'refresh_token',
    value: 'refresh_token'
  },
  {
    label: 'password',
    value: 'password'
  },
  {
    label: 'authorization_code',
    value: 'authorization_code'
  },
  {
    label: 'mobile',
    value: 'mobile'
  },
  {
    label: 'implicit',
    value: 'implicit'
  },
  {
    label: 'client_credentials',
    value: 'client_credentials'
  }
]
