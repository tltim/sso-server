export const tableObj = {
  columns: [
    {
      title: '应用名称',
      field: 'name',
      type: 'input'
    },
    {
      title: '唯一编码',
      field: 'code',
      type: 'input'

    },
    {
      title: '别名',
      field: 'alias',
      type: 'input'
    },
    {
      title: '应用地址',
      field: 'url',
      type: 'input'
    },
    {
      title: '状态',
      type: 'input',
      field: 'status'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
