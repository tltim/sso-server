import { statusTypeList } from '@/dict'
export const tableObj = {
  columns: [
    {
      title: 'ID号',
      field: 'id',
      sortable: true,
      type: 'input',
      placeholder: '',
    },
    {
      title: '用户名',
      field: 'username',
      sortable: true,
      type: 'input',
      placeholder: '',
    },
    {
      title: '真实姓名',
      field: 'truename',
      sortable: true,
      type: 'input',
      placeholder: '',
    },
    {
      title: '头像',
      field: 'avatar',
      hidden: true,
      slot: true
    },
    {
      title: '状态',
      field: 'status',
      type: 'select',
      sortable: true,
      slot: true,
      options: statusTypeList
    },
    {
      title: '手机号',
      width: '10%',
      type: 'input',
      sorter: true,
      field: 'phone'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
