export const tableObj = {
  hidden: true,
  columns: [
    {
      title: '权限名称',
      field: 'name',
      type: 'input',
      placeholder: '',
      search: false,
      treeNode: true
    },
    {
      title: '权限编码',
      field: 'perms',
    },
    {
      title: '所属应用',
      field: 'application',
      type: 'input',
      placeholder: '',
    },
    {
      title: '权限类型',
      field: 'type',
      slot: true
    },
    {
      title: '权限状态',
      field: 'status',
    },
    {
      title: '操作',
      width: '200px',
      field: 'action',
      slot: true
    }
  ]
}
