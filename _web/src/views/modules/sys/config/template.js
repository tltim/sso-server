
export const tableObj = {
  columns: [
    {
      title: '参数',
      field: 'paramKey'
    },
    {
      title: '参数值',
      field: 'paramValue'
    },
    {
      title: '状态',
      field: 'status',
      slot: true,
      type: 'select',
      options: [
        {
          label: '禁用',
          value: '0'
        },
        {
          label: '正常',
          value: '1'
        }
      ]
    },
    {
      title: '备注',
      field: 'remark'
    },
    {
      title: '操作',
      field: 'action',
      slot: true,
      hidden: true,
    }
  ]
}

export const formObj = {
  formkey: 'id',
  columns: [
    {
      label: '参数',
      value: 'paramKey',
      required: true,
      message: '请输入参数',
      placeholder: '请输入参数',
      type: 'input'
    },
    {
      label: '参数值',
      value: 'paramValue',
      required: true,
      message: '请输入参数值',
      placeholder: '请输入参数值',
      type: 'input'
    },
    {
      label: '状态',
      value: 'status',
      required: true,
      message: '请选择状态',
      placeholder: '请选择状态',
      type: 'select',
      options: [
        {
          label: '禁用',
          value: '0'
        },
        {
          label: '正常',
          value: '1'
        }
      ]
    },
    {
      label: '备注',
      value: 'remark',
      required: true,
      message: '请输入备注',
      placeholder: '请输入备注',
      type: 'input'
    },
  ]

}
