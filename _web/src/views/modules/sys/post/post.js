
export const tableObj = {
  columns: [
    {
      title: '岗位名称',
      sorter: true,
      field: 'name',
      type: 'input',
      slot: true
    },
    {
      title: '岗位代码',
      sorter: true,
      field: 'code',
      type: 'input',
    },
    {
      title: '岗位类型',
      sorter: true,
      field: 'type',
      type: 'select',
      options: [
        {
          value: '高管',
          label: '高管'
        }, {
          value: '中层',
          label: '中层'
        }, {
          value: '基层',
          label: '基层'
        }
      ]
    },
    {
      title: '数据权限类型',
      sorter: true,
      field: 'dsType'
    },
    {
      title: '系统内置',
      sorter: true,
      field: 'isSystem',
      slot: true
    },
    {
      title: '排序',
      sorter: true,
      field: 'orderNum'
    },
    {
      title: '备注',
      width: '500px',
      field: 'remark'
    },
    {
      title: '操作',
      width: '200px',
      field: 'action',
      slot: true
    }
  ]
}

export const formObj = {
  formkey: 'id',
  columns: [
    {
      label: '岗位名称',
      value: 'name',
      required: true,
      message: '请输入岗位名称',
      placeholder: '请输入岗位名称',
      type: 'input'
    },
    {
      label: '岗位代码',
      value: 'code',
      required: true,
      message: '请输入岗位代码',
      placeholder: '请输入岗位代码',
      type: 'input'
    },
    {
      label: '排序号',
      value: 'orderNum',
      required: true,
      message: '请输入排序号',
      placeholder: '请输入排序号',
      type: 'input'
    },
    {
      label: '岗位类别',
      value: 'type',
      required: true,
      message: '请选择岗位类型',
      placeholder: '请选择岗位类型',
      type: 'select',
      options: [
        {
          value: '高管',
          label: '高管'
        }, {
          value: '中层',
          label: '中层'
        }, {
          value: '基层',
          label: '基层'
        }
      ]
    },
    {
      label: '数据权限',
      value: 'dsType',
      required: true,
      message: '请输入选择数据权限',
      type: 'select',
      placeholder: '请输入选择数据权限',
      options: [
        {
          label: '全部',
          value: '0'
        },
        {
          label: '本级',
          value: '3'
        },
        {
          label: '本级及子级',
          value: '2'
        }
      ]
    },
    {
      label: '备注',
      value: 'remark',
      type: 'textarea',
      placeholder: '请输入备注',
      rows: 4
    }
  ]

}
