export const tableObj = {
  columns: [
    {
      title: '角色名称',
      field: 'roleName'
    },
    {
      title: '角色代码',
      field: 'roleCode'
    },
    {
      title: '系统内置',
      field: 'isSystem',
      slot: true,
      hidden: true,
    },
    {
      title: '创建时间',
      field: 'createTime',
      hidden: true,
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
