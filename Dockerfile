# 基础镜像，当前新镜像是基于哪个镜像
FROM java:8-jre
# RUN：容器构建时需要运行的命令
RUN echo "Asia/Shanghai" > /etc/timezone;dpkg-reconfigure -f noninteractive tzdata
# ADD：将宿主机目录下的文件拷贝到镜像且 ADD 命令会自动处理 URL 和解压 tar 压缩包
WORKDIR /
ADD ./target/sso-server-*.jar sso-server.jar
# 利用 chmod 可以藉以控制文件如何被他人所调用。
RUN chmod +x sso-server.jar
# EXPOSE：当前容器对外暴露的端口
EXPOSE 9999
# CMD：指定一个容器启动时要运行的命令。Dockerfile 中可以有多个 CMD 命令，
# 但只有最后一个生效，CMD 会被 docker run 之后的参数替换
CMD ["java", "-Xmx500m",  "-jar", "sso-server.jar", "--spring.profiles.active=prod"]